use crate::message::token::{LineToken, Token, TokenError};
use crate::output::Output;
use std::io::{self, Write};

#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct Params {
  init: Vec<Token>,
  last: Option<LineToken>,
}

impl Params {
  pub fn is_empty(&self) -> bool {
    self.init.is_empty() && self.last.is_none()
  }

  pub fn new(init: Vec<Token>, last: Option<LineToken>) -> Self {
    Self { init, last }
  }

  pub fn from_iter<I: AsRef<str>>(it: impl IntoIterator<Item = I>) -> Result<Self, TokenError> {
    let items: Vec<I> = it.into_iter().collect();
    let mut this = Self::new(vec![], None);
    match items.split_last() {
      None => {}
      Some((last, most)) => {
        for s in most.iter() {
          this.init.push(s.as_ref().parse()?);
        }
        if let Ok(r) = last.as_ref().parse() {
          this.init.push(r);
        } else {
          this.last = Some(last.as_ref().parse()?);
        }
      }
    }
    Ok(this)
  }
}

impl Output for Params {
  fn write_to<W: Write>(&self, w: &mut W) -> io::Result<()> {
    for (i, p) in self.init.iter().enumerate() {
      if i > 0 {
        w.write_all(b" ")?;
      }
      p.write_to(w)?;
    }
    if let Some(ref p) = self.last {
      if !self.init.is_empty() {
        w.write_all(b" ")?;
      }
      p.write_to(w)?;
    }
    Ok(())
  }
}

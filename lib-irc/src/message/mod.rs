pub mod params;
pub mod token;

use crate::output::Output;
pub use params::Params;
use std::io::{self, Write};
pub use token::{Token, TokenError};

#[derive(Debug)]
pub struct Tag(pub String, pub String);

#[derive(Debug)]
pub struct Message {
  pub tags: Vec<Tag>,
  pub prefix: Option<Token>,
  pub cmd: Token,
  pub params: Params,
}

impl Message {
  pub fn new(
    cmd: impl AsRef<str>,
    params: impl IntoIterator<Item = impl AsRef<str>>,
  ) -> Result<Self, TokenError> {
    Ok(Self {
      tags: Vec::new(),
      prefix: None,
      cmd: cmd.as_ref().parse()?,
      params: Params::from_iter(params)?,
    })
  }
}

impl Output for Message {
  fn write_to<W: Write>(&self, w: &mut W) -> io::Result<()> {
    if !self.tags.is_empty() {
      w.write_all(b"@")?;
      for (idx, t) in self.tags.iter().enumerate() {
        if idx > 0 {
          w.write_all(b";")?;
        }
        t.write_to(w)?;
      }
      w.write_all(b" ")?;
    }

    if let Some(ref p) = self.prefix {
      w.write_all(b":")?;
      p.write_to(w)?;
      w.write_all(b" ")?;
    }

    self.cmd.write_to(w)?;

    if !self.params.is_empty() {
      w.write_all(b" ")?;
      self.params.write_to(w)?;
    }

    w.write_all(b"\r\n")
  }
}

impl Output for Tag {
  fn write_to<W: Write>(&self, w: &mut W) -> io::Result<()> {
    write!(w, "{}={}", self.0, self.1)
  }
}

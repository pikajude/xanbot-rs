use crate::message::{
  params::Params,
  token::{LineToken, Token},
  Message, Tag,
};
use nom::{
  bytes::streaming::{tag, take_while, take_while1},
  character::streaming::{char, space0},
  combinator::{all_consuming, map, map_res, opt},
  multi::separated_nonempty_list,
  sequence::{delimited, preceded, separated_pair, terminated, tuple},
  IResult,
};

fn as_string<'a, F>(comb: F) -> impl Fn(&'a [u8]) -> IResult<&'a [u8], String>
where
  F: Fn(&'a [u8]) -> IResult<&'a [u8], &'a [u8]>,
{
  map_res(comb, |u| String::from_utf8(u.to_vec()))
}

fn irc_tag(input: &[u8]) -> IResult<&[u8], Tag> {
  map(
    separated_pair(
      as_string(take_while(|c| !b" ;=".contains(&c))),
      opt(char('=')),
      as_string(take_while(|c| !b" ;".contains(&c))),
    ),
    |(tn, tv)| Tag(tn, tv),
  )(input)
}

fn prefix(input: &[u8]) -> IResult<&[u8], Token> {
  preceded(char(':'), simple_token)(input)
}

fn simple_token(input: &[u8]) -> IResult<&[u8], Token> {
  map(
    terminated(as_string(take_while1(|c| !b" \r\n\t".contains(&c))), space0),
    Token::unsafe_from,
  )(input)
}

fn parameters(mut input: &[u8]) -> IResult<&[u8], Params> {
  let mut elems = Vec::new();
  let mut last_elem = None;
  while let Some(ch) = input.iter().next() {
    if *ch == b'\r' {
      break;
    }

    if *ch == b':' {
      let (i, p0) = as_string(take_while(|c| c != b'\r'))(&input[1..])?;
      input = i;
      last_elem = Some(LineToken::unsafe_from(p0));
      break;
    }

    let (i2, tok) = simple_token(input)?;
    input = i2;
    elems.push(tok);
  }

  Ok((input, Params::new(elems, last_elem)))
}

/// Parse a bytestring into a [Message](Message).
pub fn message(input: &[u8]) -> IResult<&[u8], Message> {
  map(
    tuple((
      opt(delimited(
        char('@'),
        separated_nonempty_list(char(';'), irc_tag),
        space0,
      )),
      opt(prefix),
      simple_token,
      parameters,
      tag("\r\n"),
    )),
    |(tags, prefix, cmd, params, _)| Message {
      tags: tags.unwrap_or_default(),
      prefix,
      cmd,
      params,
    },
  )(input)
}

impl std::str::FromStr for Message {
  type Err = String;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    match all_consuming(message)(s.as_bytes()) {
      Ok((_, m)) => Ok(m),
      Err(e) => Err(format!("{:?}", e)),
    }
  }
}

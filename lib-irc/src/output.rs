use std::io;

pub trait Output {
  fn write_to<W: io::Write>(&self, writer: &mut W) -> io::Result<()>;
}

use std::fmt;

#[allow(clippy::module_name_repetitions)]
#[derive(Debug)]
pub enum CodecError {
  IoError(std::io::Error),
  ParseError(Vec<u8>, nom::error::ErrorKind),
}

impl fmt::Display for CodecError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      Self::IoError(e) => write!(f, "{}", e),
      Self::ParseError(u, e) => write!(f, "Parse error(input = {:?}, error = {:?})", u, e),
    }
  }
}

impl std::error::Error for CodecError {}

impl From<std::io::Error> for CodecError {
  fn from(i: std::io::Error) -> Self {
    Self::IoError(i)
  }
}

use crate::output::Output;
use std::fmt;
use std::io::{self, Write};
use std::str::FromStr;

#[allow(clippy::module_name_repetitions)]
#[derive(Debug)]
pub struct TokenError(&'static str);

impl fmt::Display for TokenError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "Token parse failure: {}", self.0)
  }
}

impl std::error::Error for TokenError {}

macro_rules! token {
  ( $e:ident , $chars:expr ) => {
    #[derive(Clone, Debug, Default, PartialEq, Eq)]
    pub struct $e(String);

    impl $e {
      pub fn unsafe_from(s: impl Into<String>) -> Self {
        Self(s.into())
      }
    }

    impl FromStr for $e {
      type Err = TokenError;

      fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.contains(|c| $chars.contains(c)) {
          Err(TokenError(concat!(
            stringify!($e),
            " may not contain any character in class ",
            stringify!($chars)
          )))
        } else {
          Ok(Self(String::from(s)))
        }
      }
    }

    impl Output for $e {
      fn write_to<W: Write>(&self, w: &mut W) -> io::Result<()> {
        w.write_all(self.0.as_bytes())
      }
    }
  };
}

token!(Token, " \t\r\n");
token!(LineToken, "\r\n");

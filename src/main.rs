#![feature(async_closure)]
#![warn(clippy::all, clippy::pedantic, clippy::nursery)]

mod auth;
mod bot;
mod db;
mod util {
  pub mod uriencode;
}

use bot::Bot;
use db::Db;

use std::error::Error;
use std::sync::Arc;

async fn run() -> Result<(), Box<dyn Error>> {
  let db = Db::open()?;
  let tok = match db.get_token()? {
    Some(t) => t,
    None => auth::authorize().await?,
  };

  let b = Arc::new(Bot::new(tok).await?);

  b.write(vec![
    irc::Message::new("PASS", &[format!("oauth:{}", std::env::var("PASS")?)])?,
    irc::Message::new("NICK", &["xan666bot"])?,
  ])
  .await?;

  while let Some(x) = b.read().await? {
    println!("{:?}", x);
  }

  println!("Connection closed.");

  Ok(())
}

#[tokio::main]
async fn main() {
  match run().await {
    Ok(()) => (),
    Err(e) => println!("{}", e),
  }
}

mod error;

use crate::{message::Message, output::Output, parse};
use bytes::BytesMut;
use error::CodecError;
use tokio::codec::{Decoder, Encoder};

pub struct Codec;

impl Decoder for Codec {
  type Item = Message;
  type Error = CodecError;

  fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
    use nom::Err::*;

    let srcptr = src.as_ptr() as usize;

    match parse::message(src) {
      Ok((slice, m)) => {
        let newptr = slice.as_ptr() as usize;
        src.advance(newptr - srcptr);
        Ok(Some(m))
      }
      Err(Failure((buf, m))) => Err(CodecError::ParseError(buf.to_vec(), m)),

      // for Error or Incomplete, subsequent parses may succeed. request more data
      Err(_) => Ok(None),
    }
  }
}

impl Encoder for Codec {
  type Item = Message;
  type Error = std::io::Error;

  fn encode(&mut self, item: Self::Item, dst: &mut BytesMut) -> Result<(), Self::Error> {
    let mut bytes: Vec<u8> = Vec::new();
    item.write_to(&mut bytes)?;
    dst.extend_from_slice(&bytes);
    Ok(())
  }
}

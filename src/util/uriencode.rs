use bytes::Bytes;

fn encode(s: &str) -> Vec<u8> {
  let mut buf = Vec::with_capacity(s.len());
  for c in s.bytes() {
    match c {
      b'A'..=b'Z' | b'a'..=b'z' | b'0'..=b'9' | b'*' | b'-' | b'.' | b'_' => buf.push(c),
      b' ' => buf.push(b'+'),
      x => buf.extend(format!("%{:X}", x).as_bytes()),
    }
  }
  buf
}

pub fn qstring<S: AsRef<str>, S2: AsRef<str>, I: IntoIterator<Item = (S, S2)>>(its: I) -> Bytes {
  let mut buf = Bytes::new();
  for (i, (k, v)) in its.into_iter().enumerate() {
    if i == 0 {
      buf.extend(b"?");
    } else {
      buf.extend(b"&");
    }
    buf.extend(encode(k.as_ref()));
    let vstr = v.as_ref();
    if !vstr.is_empty() {
      buf.extend(b"=");
      buf.extend(encode(vstr));
    }
  }
  buf
}

use futures::lock::Mutex;
use futures::sink::SinkExt;
use futures::stream::{SplitSink, SplitStream, StreamExt, TryStreamExt};
use tokio::codec::Framed;
use tokio::net::TcpStream;
use tokio_tls::{TlsConnector, TlsStream};

type IStream = Framed<TlsStream<TcpStream>, irc::Codec>;

pub struct Bot {
  stream: Mutex<SplitStream<IStream>>,
  sink: Mutex<SplitSink<IStream, irc::Message>>,
  _token: String,
}

impl Bot {
  const HOSTNAME: &'static str = "irc.chat.twitch.tv";
  const PORT: u16 = 6697;

  pub async fn new(token: String) -> Result<Self, Box<dyn std::error::Error>> {
    let s = Framed::new(
      TlsConnector::from(native_tls::TlsConnector::new()?)
        .connect(
          Self::HOSTNAME,
          TcpStream::connect((Self::HOSTNAME, Self::PORT)).await?,
        )
        .await?,
      irc::Codec,
    );
    let (sink, stream) = s.split();
    Ok(Self {
      _token: token,
      stream: Mutex::new(stream),
      sink: Mutex::new(sink),
    })
  }

  pub async fn read(
    &self,
  ) -> Result<Option<irc::Message>, <irc::Codec as tokio::codec::Decoder>::Error> {
    self.stream.lock().await.try_next().await
  }

  pub async fn write<I: IntoIterator<Item = irc::Message>>(&self, msgs: I) -> std::io::Result<()> {
    self
      .sink
      .lock()
      .await
      .send_all(&mut futures::stream::iter(msgs))
      .await
  }
}

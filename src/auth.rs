use bytes::Bytes;
use futures::channel::oneshot::{self, Canceled, Sender};
use futures::lock::Mutex;
use hyper::server::conn::AddrStream;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Error, Request, Response, Server};
use std::sync::Arc;

pub async fn authorize() -> Result<String, Canceled> {
  let mut path = Bytes::from(&b"/oauth2/authorize"[..]);
  path.extend(crate::util::uriencode::qstring(vec![
    ("client_id", "z92ktp9wj39vy8wc0t8q2k52q41pb3"),
    ("redirect_uri", "http://104.238.180.192:8080/callback"),
    ("response_type", "code"),
    ("scope", "user:read:email"),
  ]));

  println!(
    "To authorize xanbot, go to {}",
    http::uri::Builder::new()
      .scheme("https")
      .authority("id.twitch.tv")
      .path_and_query(path)
      .build()
      .unwrap(),
  );

  let (send, recv) = oneshot::channel();

  tokio::spawn(async move {
    token_wait(send).await.unwrap();
  });

  recv.await
}

async fn token_wait(chan: Sender<String>) -> Result<(), Error> {
  let _shared_chan = Arc::new(Mutex::new(chan));

  Server::bind(&([0, 0, 0, 0], 8080).into())
    .serve(make_service_fn(async move |_: &AddrStream| {
      Ok::<_, Error>(service_fn(async move |req: Request<Body>| {
        Ok::<_, Error>(Response::new(Body::from(format!("Hello, {}!", req.uri()))))
      }))
    }))
    .await
}

create table if not exists _token (
  id integer primary key unique,
  token text(30)
);

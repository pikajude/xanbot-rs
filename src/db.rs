use rusqlite::{params, Connection, Error, Result};

pub struct Db(Connection);

impl Db {
  const ID: u8 = 0;

  pub fn open() -> Result<Self> {
    let conn = Connection::open("xanbot.sqlite")?;
    conn.execute_batch(include_str!("init.sql"))?;
    Ok(Self(conn))
  }

  pub fn get_token(&self) -> Result<Option<String>> {
    match self.0.query_row(
      "SELECT token FROM _token WHERE id = ?",
      params![Self::ID],
      |r| r.get("token"),
    ) {
      Err(Error::QueryReturnedNoRows) => Ok(None),
      Ok(s) => Ok(Some(s)),
      Err(e) => Err(e),
    }
  }

  pub fn _put_token(&self, tok: &str) -> Result<()> {
    match self.0.execute(
      "INSERT OR REPLACE INTO _token(id, token) VALUES (?, ?)",
      params![Self::ID, tok],
    )? {
      1 => Ok(()),
      n => Err(Error::StatementChangedRows(n)),
    }
  }
}
